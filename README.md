# pyside6-debian

Small repo to automate the creation and upload of PySide6 debian packages using [wheel2deb](https://github.com/upciti/wheel2deb/)

## Usage

- `build.sh`: First script. Download PySide6 wheels, applies some patches.
- `upload.sh`: Converts the source to PPA uploadable DSCs, and uploads them to a supplied PPA.
- `make-debs.sh`: Converts the source packages into debs.
