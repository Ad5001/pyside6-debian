#!/bin/bash

DISTRIBUTION=${DISTRIBUTION:-noble}
REVISION=${REVISION:-1}

rm -rf output
rm -rf *.whl
rm wheel2deb.yml

# Download latest weels
python3 -m pip wheel PySide6-Essentials
python3 -m pip wheel PySide6-Addons

# Create wheel for distribution
cp wheel2deb.src.yml wheel2deb.yml
sed -i -e "s/\\[DIST\\]/$DISTRIBUTION/g" wheel2deb.yml

wheel2deb convert -c ./wheel2deb.yml

echo -e "\n[Writing modifications]\n"

for i in output/*; do
    cp copyright "$i/debian" # Add copyrights
    perl -i -p0e 's/python3 \(<< 3.(\d)+\),//gm' "$i/debian/control" # Remove upper limit from control.
    sed -i "s/stable;/$DISTRIBUTION;/g" "$i/debian/changelog" # Change distribution.
    sed -i "s/1~w2d0/$REVISION/g" "$i/debian/changelog" # Change suffix
    mv "$i" "${i/1~w2d0/$REVISION}"
done

# Removing android deploy libs because they cause issues at installation.
rm -rf output/python3-pyside6-essentials_*_amd64/src/PySide6/scripts/deploy_lib/android

# Remove conflicting files between Essentials and Addons
rm output/python3-pyside6-addons_*_amd64/src/PySide6/{__init__.py,_config.py,_git_pyside_version.py,__feature__.pyi,py.typed}

for file in output/python3-pyside6-essentials_*_amd64/src/PySide6/Qt*.pyi; do
    if [ ! -f "${file%.*}.abi3.so" ]; then 
        echo "Removing $file..."
        rm "$file"
    fi
done

for file in output/python3-pyside6-addons_*_amd64/src/PySide6/Qt*.pyi; do
    if [ ! -f "${file%.*}.abi3.so" ]; then 
        echo "Removing $file..."
        rm "$file"
    fi
done

# Removing conflictual directories
rm -rf output/python3-pyside6-addons_*_amd64/src/shiboken6*
rm -rf output/python3-pyside6-essentials_*_amd64/src/shiboken6*
rm -rf output/python3-shiboken6_*_amd64/src/PySide6

# Checking for remaining conflicts.
cd output/python3-pyside6-essentials_*_amd64 || exit
for file in $(find src); do
    if [ -f ../python3-pyside6-addons_*_amd64/$file ]; then
        echo "$file is dupplicated";
        exit 1
    fi
done

function filter() { cat "$1" | awk "$2" > "$1.mv"; mv "$1.mv" "$1"; }

# filter output/python3-shiboken6_*_amd64/debian/install '/^src\/shiboken6/ { print $0; }'
# filter output/python3-pyside6-essentials_*_amd64/debian/install '/^src\/PySide6/ { print $0; }'
# filter output/python3-pyside6-addons_*_amd64/debian/install '/^src\/PySide6/ { print $0; }'
