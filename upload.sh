#!/bin/bash

PPA_ARCHIVE="ppa:ad5001/logarithmplotter"

# Build and sign the packages.
for i in output/*_amd64; do
    cd "$i"
    debuild -S -sa -k"mail@ad5001.eu"
    cd ../../
done 

# Upload every package
cd output
for i in *_source.changes; do
    dput $PPA_ARCHIVE "./$i"
done
